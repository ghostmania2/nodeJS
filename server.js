
var User = require('./user');
var log = require('./logger')(module);

function run(){
	var vasya = new User("Vasya");
	var fedor = new User("Fedor");

	vasya.hello(fedor)
	log("user.js is required");
}

if (module.parent) {
	exports.run = run;
} else {
	run();
}
